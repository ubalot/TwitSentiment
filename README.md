# TwitSentiment
Read tweet messages with different colors related to their sentiment


## Install
```bash
npm install
```

## Run the script
```bash
node index.js -k <keyword> -c <count>
```

