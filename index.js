#!/usr/bin/env node
'use strict'

const Twit = require('twit')
const dotenv = require('dotenv')
const sentiment = require('sentiment')
const colors = require('colors/safe')
const ArgumentParser = require('./node_modules/argparse/lib/argparse').ArgumentParser

/*
 * Argument parsing configuration
 */

const parser = new ArgumentParser({
  version: '0.0.1',
  addHelp:true,
  description: 'Argparse example'
});

const validArgs = [
  {
    name: [ '-k', '--keyword' ],
    help: { help: 'target keyword' }
  },
  {
    name: [ '-c', '--count' ],
    help: { help: 'number of tweets' }
  }
]

validArgs.map(arg => parser.addArgument(arg.name, arg.help))

const args = parser.parseArgs()


/*
 * Twitter client configuration
 */

dotenv.config()

const { CONSUMER_KEY
      , CONSUMER_SECRET
      , ACCESS_TOKEN
      , ACCESS_TOKEN_SECRET
      } = process.env

const twit_config = {
  consumer_key: CONSUMER_KEY,
  consumer_secret: CONSUMER_SECRET,
  access_token: ACCESS_TOKEN,
  access_token_secret: ACCESS_TOKEN_SECRET,
  timeout_ms: 60*1000
}

const twitApi = new Twit(twit_config)



function get_tweets(q, count) {

  function get_text(tweet) {
    // Get content even in case of retweet. Return message withoud links.
    const txt = tweet.retweeted_status ? tweet.retweeted_status.full_text : tweet.full_text
    return txt.split(/ |\n/).filter(v => !v.startsWith('http')).join(' ')
   }

  return twitApi.get('search/tweets', {q, count, 'tweet_mode': 'extended'})
    .then(tweets => tweets.data.statuses.map(get_text))
    .catch(err => res = err)
}

async function main(args) {
  const { keyword, count } = args
  const tweets = await get_tweets(keyword, count)
  for (let tweet of tweets) {
    const tweetSentiment = sentiment(tweet)
    const score = tweetSentiment ? tweetSentiment.comparative : 0
    tweet = `${tweet}\n`
    if (score > 0) {
      tweet = colors.green(tweet)
    } else if (score < 0) {
      tweet = colors.red(tweet)
    } else {
      tweet = colors.blue(tweet)
    }
    console.log(tweet)
  }
}

main(args)
